package com.flycms;


import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.service.IWeightService;
import com.flycms.modules.elastic.domain.dto.SearchDTO;
import com.flycms.modules.elastic.service.impl.ElasticSearchServiceImpl;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.modules.statistics.service.IStatisticsAccessPageService;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringDataElasticsearchFindTest {


    @Autowired
    private ElasticSearchServiceImpl elasticSearchServiceImpl;


    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IStatisticsAccessPageService statisticsAccessPageService;
    @Autowired
    private IWeightService weightService;


    /**
     * 查询全部
     */
    @Test
    // 创建索引
    public void createIndexStu() {
        statisticsAccessPageService.insertStatisticsAccessPage();
        weightService.updateTopicWeight();
    }

    @Test
    public void setEmail() throws Exception {
        Pager<SearchDTO> kk= elasticSearchServiceImpl.relevantSearch(1, 10, "美", "530734146658500608","title", "content");
        kk.getList().forEach(entity -> {
            System.err.println(entity.getId()+"------------"+entity.getTitle());
                });

    }

    //获取中文的首字母
    @Test
    public void testPinyin() throws BadHanyuPinyinOutputFormatCombination {
        String name = "x互xx";
        char[] charArray = name.toCharArray();
        StringBuilder pinyin = new StringBuilder();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        // 设置大小写格式
        defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        // 设置声调格式：
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < charArray.length; i++) {
            //匹配中文,非中文转换会转换成null
            if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                String[] hanyuPinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                if (hanyuPinyinStringArray != null) {
                    pinyin.append(hanyuPinyinStringArray[0].charAt(0));
                }
            }
        }
        System.err.println(pinyin);
    }

}
