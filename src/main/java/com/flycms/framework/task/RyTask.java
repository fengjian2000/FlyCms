package com.flycms.framework.task;

import com.flycms.modules.data.service.IWeightService;
import com.flycms.modules.statistics.service.IStatisticsAccessPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.flycms.common.utils.StrUtils;

/**
 * 定时任务调度测试
 * 
 * @author kaifei sun
 */
@Component("ryTask")
public class RyTask {
    @Autowired
    private IStatisticsAccessPageService statisticsAccessPageService;
    @Autowired
    private IWeightService weightService;

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StrUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        statisticsAccessPageService.insertStatisticsAccessPage();
        weightService.updateTopicWeight();
        System.out.println("执行无参方法");
    }
}
