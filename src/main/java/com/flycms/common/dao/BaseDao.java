package com.flycms.common.dao;

import com.flycms.common.utils.page.Pager;

import java.util.List;
import java.util.Map;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 基础Dao(还需在XML文件里，有对应的SQL语句)
 *
 * @author 孙开飞
 * @email 79678111@qq.com
 * @date 2016年9月18日 上午9:31:36
 */
public interface BaseDao<T> {

    int save(T t);

    void save(Map<String, Object> map);

    void saveBatch(List<T> list);

    int update(T t);

    int update(Map<String, Object> map);

    int deleteById(Object id);

    int deleteById(Map<String, Object> map);

    int deleteBatch(Object[] id);

    T findById(Object id);

    int selectTotal(Object pager);

    List<T> selectList(Object pager);

    int queryTotal(Map<String, Object> map);

    List<T> queryList(Map<String, Object> map);

    //按条件查询记录条数
    int queryTotal(Pager pager);

    //按条件查询列表
    List<T> queryList(Pager pager);

}
