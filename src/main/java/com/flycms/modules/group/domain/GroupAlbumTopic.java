package com.flycms.modules.group.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 小组专辑帖子关联对象 fly_group_album_topic
 * 
 * @author admin
 * @date 2020-12-16
 */
@Data
public class GroupAlbumTopic extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 专辑ID */
    private Long albumId;
    /** 帖子ID */
    private Long topicId;
    /** 章节名称 */
    private String chapter;
    /** 内容排序 */
    private Integer sortOrder;
}
