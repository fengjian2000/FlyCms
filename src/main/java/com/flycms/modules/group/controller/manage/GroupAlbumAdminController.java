package com.flycms.modules.group.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupAlbum;
import com.flycms.modules.group.domain.dto.GroupAlbumDTO;
import com.flycms.modules.group.service.IGroupAlbumService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 小组帖子专辑Controller
 * 
 * @author admin
 * @date 2020-12-16
 */
@RestController
@RequestMapping("/system/group/album")
public class GroupAlbumAdminController extends BaseController
{
    @Autowired
    private IGroupAlbumService groupAlbumService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增小组帖子专辑
     */
    @PreAuthorize("@ss.hasPermi('group:album:add')")
    @Log(title = "小组帖子专辑", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupAlbum groupAlbum)
    {
        if (UserConstants.NOT_UNIQUE.equals(groupAlbumService.checkGroupAlbumAlbumNameUnique(groupAlbum.getId(),groupAlbum.getAlbumName())))
        {
            return AjaxResult.error("新增小组帖子专辑'" + groupAlbum.getAlbumName() + "'失败，专辑名字已存在");
        }
        return toAjax(groupAlbumService.insertGroupAlbum(groupAlbum));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组帖子专辑
     */
    @PreAuthorize("@ss.hasPermi('group:album:remove')")
    @Log(title = "小组帖子专辑", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupAlbumService.deleteGroupAlbumByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组帖子专辑
     */
    @PreAuthorize("@ss.hasPermi('group:album:edit')")
    @Log(title = "小组帖子专辑", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupAlbum groupAlbum)
    {
    if (UserConstants.NOT_UNIQUE.equals(groupAlbumService.checkGroupAlbumAlbumNameUnique(groupAlbum.getId(),groupAlbum.getAlbumName())))
        {
            return AjaxResult.error("新增小组帖子专辑'" + groupAlbum.getAlbumName() + "'失败，专辑名字已存在");
        }
        return toAjax(groupAlbumService.updateGroupAlbum(groupAlbum));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询小组帖子专辑列表
     */
    @PreAuthorize("@ss.hasPermi('group:album:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupAlbum groupAlbum,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupAlbumDTO> pager = groupAlbumService.selectGroupAlbumPager(groupAlbum, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出小组帖子专辑列表
     */
    @PreAuthorize("@ss.hasPermi('group:album:export')")
    @Log(title = "小组帖子专辑", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupAlbum groupAlbum)
    {
        List<GroupAlbumDTO> groupAlbumList = groupAlbumService.exportGroupAlbumList(groupAlbum);
        ExcelUtil<GroupAlbumDTO> util = new ExcelUtil<GroupAlbumDTO>(GroupAlbumDTO.class);
        return util.exportExcel(groupAlbumList, "album");
    }

    /**
     * 获取小组帖子专辑详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:album:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupAlbumService.findGroupAlbumById(id));
    }

}
