package com.flycms.modules.user.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户和app关联对象 fly_user_app_merge
 * 
 * @author admin
 * @date 2020-05-31
 */
@Data
public class UserAppMerge extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** APP的id */
    private Long appId;
    /** 用户id */
    private Long userId;
}
