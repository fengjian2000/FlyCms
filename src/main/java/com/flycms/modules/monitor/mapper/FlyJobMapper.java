package com.flycms.modules.monitor.mapper;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.FlyJob;
import org.springframework.stereotype.Repository;

/**
 * 调度任务信息 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyJobMapper
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增调度任务信息
     *
     * @param job 调度任务信息
     * @return 结果
     */
    public int insertJob(FlyJob job);

    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 通过调度ID删除调度任务信息
     *
     * @param jobId 调度ID
     * @return 结果
     */
    public int deleteJobById(Long jobId);

    /**
     * 批量删除调度任务信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJobByIds(Long[] ids);
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改调度任务信息
     *
     * @param job 调度任务信息
     * @return 结果
     */
    public int updateJob(FlyJob job);

    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////

    /**
     * 通过调度ID查询调度任务信息
     *
     * @param jobId 调度ID
     * @return 角色对象信息
     */
    public FlyJob selectJobById(Long jobId);

    /**
     * 查询调度任务日志集合 总数
     *
     * @param pager
     * @return
     */
    public int queryJobTotal(Pager pager);

    /**
     * 查询调度任务日志集合
     * 
     * @param pager 调度信息
     * @return 操作日志集合
     */
    public List<FlyJob> selectJobPager(Pager pager);

    /**
     * 查询需要导出调度任务
     *
     * @param job 调度任务信息
     * @return 调度任务列表
     */
    public List<FlyJob> exportJobList(FlyJob job);


    /**
     * 查询所有调度任务
     *
     * @return 调度任务列表
     */
    public List<FlyJob> selectJobAll();
}
