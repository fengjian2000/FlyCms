package com.flycms.modules.statistics.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 受访页面日报数据传输对象 fly_statistics_access_page
 * 
 * @author admin
 * @date 2021-01-12
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class StatisticsAccessPageDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 新客户 */
    @Excel(name = "新客户")
    private Integer isNewVisitor;
    /** 统计日期 */
    @Excel(name = "统计日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date statisticsDay;
    /** 来源类型 */
    @Excel(name = "来源类型")
    private Integer sourceType;
    /** 页面类型 */
    @Excel(name = "页面类型")
    private Integer urlType;
    /** 页面地址 */
    @Excel(name = "页面地址")
    private String url;
    /** 浏览量 */
    @Excel(name = "浏览量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer pvs;
    /** 访客数 */
    @Excel(name = "访客数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer uvs;
    /** 贡献流量 */
    @Excel(name = "贡献流量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer flows;
    /** 时长 */
    @Excel(name = "时长")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accessHoureLong;
    /** 访问次数 */
    @Excel(name = "访问次数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer onlyOnePv;
    /** 删除标识 */
    @Excel(name = "删除标识")
    private Integer deleted;

}
