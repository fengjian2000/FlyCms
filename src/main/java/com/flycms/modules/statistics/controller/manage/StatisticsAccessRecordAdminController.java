package com.flycms.modules.statistics.controller.manage;


import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.statistics.domain.StatisticsAccessRecord;
import com.flycms.modules.statistics.domain.dto.StatisticsAccessRecordDTO;
import com.flycms.modules.statistics.service.IStatisticsAccessRecordService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 访问记录Controller
 * 
 * @author admin
 * @date 2020-12-09
 */
@RestController
@RequestMapping("/system/statistics/accessRecord")
public class StatisticsAccessRecordAdminController extends BaseController
{
    @Autowired
    private IStatisticsAccessRecordService statisticsAccessRecordService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增访问记录
     */
    @PreAuthorize("@ss.hasPermi('statistics:accessRecord:add')")
    @Log(title = "访问记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StatisticsAccessRecord statisticsAccessRecord)
    {
        return toAjax(statisticsAccessRecordService.insertStatisticsAccessRecord(statisticsAccessRecord));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除访问记录
     */
    @PreAuthorize("@ss.hasPermi('statistics:accessRecord:remove')")
    @Log(title = "访问记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(statisticsAccessRecordService.deleteStatisticsAccessRecordByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改访问记录
     */
    @PreAuthorize("@ss.hasPermi('statistics:accessRecord:edit')")
    @Log(title = "访问记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StatisticsAccessRecord statisticsAccessRecord)
    {
        return toAjax(statisticsAccessRecordService.updateStatisticsAccessRecord(statisticsAccessRecord));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询访问记录列表
     */
    @PreAuthorize("@ss.hasPermi('statistics:accessRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(StatisticsAccessRecord statisticsAccessRecord,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<StatisticsAccessRecordDTO> pager = statisticsAccessRecordService.selectStatisticsAccessRecordPager(statisticsAccessRecord, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 获取访问记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('statistics:accessRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(statisticsAccessRecordService.findStatisticsAccessRecordById(id));
    }

}
