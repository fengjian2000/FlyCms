package com.flycms.modules.fullname.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 姓名搜索对象 fly_search_keyword
 * 
 * @author admin
 * @date 2020-10-13
 */
@Data
public class SearchKeyword extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 姓名 */
    private String fullName;
    /** 搜索次数 */
    private Long countView;
}
