package com.flycms.modules.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

/**
 * 行政区域数据传输对象 fly_region
 * 
 * @author admin
 * @date 2020-05-31
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class RegionDTO
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 父分类ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;
    /** 行政区域名称 */
    @Excel(name = "行政区域")
    private String regionName;
    /** 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县 */
    @Excel(name = "行政区域类型")
    private Integer regionType;
    /** 行政区域编码 */
    @Excel(name = "行政区域编码")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long regionCode;
}
