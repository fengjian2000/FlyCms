package com.flycms.modules.system.domain.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoleQueryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 角色ID */
    private Long roleId;

    /** 角色名称 */
    private String roleName;

    /** 角色状态（0正常 1停用） */
    private String status;
}
