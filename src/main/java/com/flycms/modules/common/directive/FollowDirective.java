package com.flycms.modules.common.directive;

import com.flycms.common.utils.SessionUtils;
import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.GroupUserIsaudit;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.modules.group.service.IGroupUserIsauditService;
import com.flycms.modules.group.service.IGroupUserService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.service.IUserFansService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FollowDirective extends BaseTag {

    @Autowired
    private IGroupService groupService;

    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IGroupUserService groupUserService;

    @Autowired
    private IGroupUserIsauditService groupUserIsauditService;

    @Autowired
    private IUserFansService userFansService;

    public FollowDirective() {
        super(FollowDirective.class.getName());
    }

    public Object group(Map params) {
        Long groupId = getLongParam(params, "groupId");
        //默认未专注状态
        int status = 0;
        //查询群组信息
        Group group=groupService.findGroupById(groupId);
        if(group != null){
            User user = SessionUtils.getUser();
            if(user != null){
                GroupUserIsaudit isaudit=groupUserIsauditService.findGroupUserIsauditById(user.getId(),group.getId());
                if(isaudit != null){
                    //查询是否在待审
                    if(isaudit.getStatus() == 0){
                        return status = 2;
                    }
                }
                //查询管理用户和群组关联信息
                GroupUser findgroupUser = new GroupUser();
                findgroupUser.setGroupId(group.getId());
                findgroupUser.setUserId(user.getId());
                GroupUser userDTO=groupUserService.findGroupUser(findgroupUser);
                if(userDTO != null){
                    //关注状态
                    if(userDTO.getStatus() == 1){
                        return status = 1;
                    }else{
                        return status = 0;
                    }
                }else{
                    //未关注状态
                    return status = 0;
                }
            }
        }
        return 0;
    }

    public Object user(Map params){
        Long followId = getLongParam(params, "followId");
        User user = SessionUtils.getUser();
        if(user != null){
            if(userFansService.checkUserFansUnique(followId,user.getId())){
                return 1;
            }
        }else{
            return 0;
        }
        return 0;
    }
}
