package com.flycms.modules.common.directive;

import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.service.IGroupTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PreNextPageDirective extends BaseTag {
    @Autowired
    private IGroupTopicService groupTopicService;
    public PreNextPageDirective() {
        super(PreNextPageDirective.class.getName());
    }

    public Object pre(Map params) {
        Long id = getLongParam(params, "id");
        Long typeid = getLongParam(params, "typeid");
        Long groupId = getLongParam(params, "groupId");
        GroupTopic groupTopic = new GroupTopic();
        groupTopic.setId(id);
        groupTopic.setColumnId(typeid);
        groupTopic.setGroupId(groupId);
        return groupTopicService.findGroupTopicByPrePage(groupTopic);
    }

    public Object next(Map params) {
        Long id = getLongParam(params, "id");
        Long typeid = getLongParam(params, "typeid");
        Long groupId = getLongParam(params, "groupId");
        GroupTopic groupTopic = new GroupTopic();
        groupTopic.setId(id);
        groupTopic.setColumnId(typeid);
        groupTopic.setGroupId(groupId);
        return groupTopicService.findGroupTopicByNextPage(groupTopic);
    }
}
