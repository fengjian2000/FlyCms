package com.flycms.modules.elastic.controller.front;

import com.flycms.common.validator.Sort;
import com.flycms.framework.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchController extends BaseController {



    /**
     * 查询解梦内容列表
     */
    @GetMapping(value = {"/search", "/search/{q}/p/{p}"})
    public String list(@RequestParam(value = "q", required = false) String q,
                       @RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "10") Integer pageSize,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @RequestParam(value = "p", defaultValue = "1") int p,
                       ModelMap modelMap)
    {
        modelMap.addAttribute("title", q);
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("search/index");
    }
}
