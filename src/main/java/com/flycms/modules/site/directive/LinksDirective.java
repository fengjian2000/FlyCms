package com.flycms.modules.site.directive;

import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.site.domain.SiteLinks;
import com.flycms.modules.site.service.ISiteLinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class LinksDirective extends BaseTag {

    @Autowired
    private ISiteLinksService siteLinksService;;

    public LinksDirective() {
        super(com.flycms.modules.site.directive.LinksDirective.class.getName());
    }

    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        String title = getParam(params, "title");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        SiteLinks siteLinks = new SiteLinks();
        siteLinks.setLinkName(title);
        return siteLinksService.selectSiteLinksPager(siteLinks,  pageNum, pageSize, sort, order);
    }


}