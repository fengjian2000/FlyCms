$(document).ready(function(){

    $(document).on('click', '#comm-submit', function (){
        var username=$("#username").val();
        var password=$("#password").val();
        $.ajax({
            url:'/user/login',
            type:'post',
            data:{'username':username,'password':password},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("登录成功", { shift: -1 }, function () {
                        location.href = "/";
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });

    $("#comm-submit").keydown(function(e){
        var e = e || event,
            keycode = e.which || e.keyCode;
        if (keycode==13) {
            $("#comm-submit").trigger("click");
        }
    });

});